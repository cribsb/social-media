var Bookshelf = require('Bookshelf');
var mysql = require('mysql');

var config = {
	host: 'localhost', //the host
	user: 'root', //the database user
	password: '', //the database password
	database: 'dbUsers', //the database we want to connect with
};

var DB = Bookshelf.initialize({
	client: 'mysql',
	connection: config
});

var mysqlConnection = mysql.createConnection(config);
mysqlConnection.connect();

module.exports.DB = DB;
module.exports.sql = mysqlConnection;