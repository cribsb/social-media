// vendor library
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var nodemailer = require('nodemailer');
var _ = require('Underscore');
var escape = require('escape-html');

// custom library
// model
var Model = require('./model');
var sql = require('./db').sql;

// index
var index = function(req, res, next) {
	if(!req.isAuthenticated()) {
		res.redirect('/login');
	} else {

		var user = req.user;
		var messages = [];
		var loopvar = true;

		if(user !== undefined) {
			user = user.toJSON();
		}
	  	// Get all people the current user follows the old, not working way
	  	/*sql.query('SELECT followingId FROM friendships WHERE userId = '+user.userId, function(err, rows, field){
	  		if (err) throw err;

	  		var i, j;

	  		for(i = 0; i < rows.length; i++) {
		  	  	//console.log(rows[i].followingId);
		  	  	// Get the messages posted by these people
		  	  	sql.query('SELECT content, timeStamp FROM tblmessages WHERE postedById = '+rows[i].followingId+' ORDER BY messageId DESC', function(err, rows2, field){
		  	  		if (err) throw err;

		  	  		for(j = 0; j < rows2.length; j++) {
		  	  			messages.push(rows2[j]);
		  	  			messages = _.sortBy(messages, 'timeStamp').reverse();
		  	  			//console.log(messages);
		  	  			//console.log(i);
		  	  			if(i + 1 == rows.length){
		  	  				console.log(messages.length);
							res.render('index', {title: 'Home', user: user, messages: messages});
		  				}
		  	  		}
				    //res.render('index', {title: 'Home', user: user, messages: messages});
		  	  	});
		  	  	//console.log(messages);
		  	}
		  	
		});*/
		// The new way better way
		var query = 'SELECT * FROM tblMessages WHERE postedById IN (SELECT followingId FROM friendships WHERE userId = '+user.userId+') OR postedById = '+user.userId;
		sql.query(query, function(err, rows, field){
			//console.log(rows);
			messages = _.sortBy(rows, 'timeStamp').reverse();
			res.render('index', {title: 'Home', user: user, messages: messages});
		});
  	}
};

// login
// GET
var login = function(req, res, next) {
	if(req.isAuthenticated()) res.redirect('/');
	res.render('login', {title: 'Login'});
};

// login
// POST
var loginPost = function(req, res, next) {
	passport.authenticate('local', { successRedirect: '/',
		failureRedirect: '/login'}, function(err, user, info) {
			if(err) {
				return res.render('login', {title: 'Login', errorMessage: err.message});
			} 

			if(!user) {
				return res.render('login', {title: 'Login', errorMessage: info.message});
			}
			return req.logIn(user, function(err) {
				if(err) {
					return res.render('login', {title: 'Login', errorMessage: err.message});
				} else {
					return res.redirect('/');
				}
			});
		})(req, res, next);
	};

// sign up
// GET
var signUp = function(req, res, next) {
	if(req.isAuthenticated()) {
		res.redirect('/');
	} else {
		res.render('signup', {title: 'Sign Up'});
	}
};

// sign up
// POST
var signUpPost = function(req, res, next) {
	var user = req.body;
	var usernamePromise = null;
	usernamePromise = new Model.User({username: user.username}).fetch();

	return usernamePromise.then(function(model) {
		if(model) {
			res.render('signup', {title: 'signup', errorMessage: 'username already exists'});
		} else if(user.password == user.passwordrepeat){
         //****************************************************//
         // MORE VALIDATION GOES HERE(E.G. PASSWORD VALIDATION)
         //****************************************************//
         var password = user.password;
         var hash = bcrypt.hashSync(password);

         var signUpUser = new Model.User({username: user.username, password: hash, email: user.email});

         //Mail stuff
         // create reusable transporter object using the default SMTP transport
         var transport = nodemailer.createTransport({
    		host: "smtp-mail.outlook.com", // hostname
    		secureConnection: false, // TLS requires secureConnection to be false
    		port: 587, // port for secure SMTP
    		auth: {
    			user: "chrisbogaards.cb@outlook.com",
    			pass: "cb060999"
    		},
    		tls: {
    			ciphers:'SSLv3'
    		}
    	});

		 // setup e-mail data with unicode symbols
		 var mailOptions = {
		     from: '"Cribsb" <no-reply@cribsb.com>', // sender address
		     to: user.email, // list of receivers
		     subject: 'Welcome to Cribsb', // Subject line
		     html: '<b>Hello and welcome to cribsb.</b><br><a href="http://localhost:8080/loginsite/activation.php?userid='+ user.username+'">activate your account now!</a>'// html body
		 };

		 // send mail with defined transport object
		 transport.sendMail(mailOptions, function(error, info){
		 	if(error){
		 		return console.log(error);
		 	}
		 	console.log('Message sent: ' + info.response);
		 });

		 signUpUser.save().then(function(model) {
            // login the newly registered user
            loginPost(req, res, next);
        });	
		} else {
			res.render('signup', {title: 'signup', errorMessage: 'passwords do not match'});
		}
	});
};

//post a message
var postMessage = function(req, res, next) {
	var content = req.body.msg;
	//console.log(msg);
	content = escape(content);
	if(content !== ""){
		var timeStamp = Math.floor(Date.now() / 1000);
		var postedById = req.user.attributes.userId;
		//console.log(postedById);
		sql.query("INSERT INTO tblMessages (postedById, timeStamp, content) VALUES (\""+postedById+"\",\""+timeStamp+"\",\""+content+"\")", function(err, rows, field){
			if (err){ 
				console.log('wrong!');
				res.redirect('/msgerror');
			}
		});
		res.redirect('/');
	} else {
		res.redirect('/');
	}
};

var msgerror = function(req, res, next) {
	res.render('msgerror', {title: 'Error in message'});
};

// sign out
var signOut = function(req, res, next) {
	if(!req.isAuthenticated()) {
		notFound404(req, res, next);
	} else {
		req.logout();
		res.redirect('/login');
	}
};

var searchuser = function(req, res, next) {
	//console.log(req.body.searchbox);
	//res.render('searchuser', {title: 'Search', users: ''});
	sql.query('SELECT * FROM tblusers WHERE username LIKE "%'+req.body.searchbox+'%"', function(err, rows, field){
		//console.log(rows);
		res.render('searchuser', {title: 'Search', users: rows});
	});
};

var user = function(req, res, next) {
	//console.log(req.query);
	var following = true;
	//get the messages posted by the searched user
	var messages;
	sql.query("SELECT * FROM tblmessages WHERE postedById = "+req.query.userid, function(err, rows, field){
		if(err) throw err;
		messages = rows;
	}).on('end', function(){
		sql.query("SELECT * FROM tblusers WHERE userId = "+req.query.userid, function(err, rows, field){
			if(err) throw err;
			messages = _.sortBy(messages, 'timeStamp').reverse();
			sql.query("SELECT * FROM friendships WHERE userid = '"+req.user.attributes.userId+"' AND followingId = '"+req.query.userid+"'", function(err, rows, field) {
				if (err) throw err;
				following = (rows.length >= 1)
			}).on('end', function(){
				res.render('user', {title: 'user', searcheduser: rows[0], messages: messages, following: following});
			});
		})
	});
	//get the username
	;
};

// 404 not found
var notFound404 = function(req, res, next) {
	res.status(404);
	res.render('404', {title: '404 Not Found'});
};

var follow = function(req, res, next) {
	var userid = req.query.userid;
	sql.query("INSERT INTO friendships (userId, followingId) VALUES ("+req.user.attributes.userId+", "+req.query.userid+")");
	res.redirect('/');
};

var unfollow = function(req, res, next) {
	var userid = req.query.userid;
	sql.query("DELETE FROM friendships WHERE userId='"+req.user.attributes.userId+"' AND followingId='"+req.query.userid+"'");
	res.redirect('/');
};

// export functions
/**************************************/
// index
module.exports.index = index;

// msgerror
// GET
module.exports.msgerror = msgerror;

// search user
// POST
module.exports.searchuser = searchuser;

// follow/unfollow
// GET
module.exports.follow = follow;
module.exports.unfollow = unfollow;

// user page
// GET
module.exports.user = user;

// login
// GET
module.exports.login = login;
// POST
module.exports.loginPost = loginPost;

// sign up
// GET
module.exports.signUp = signUp;
// POST
module.exports.signUpPost = signUpPost;

//posting a message
module.exports.postMessage = postMessage;

// sign out
module.exports.signOut = signOut;

// 404 not found
module.exports.notFound404 = notFound404;