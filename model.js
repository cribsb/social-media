var DB = require('./DB').DB; //import the database

var User = DB.Model.extend({
	tableName: 'tblUsers',
	idAttribute: 'userId',
});

var Message = DB.Model.extend({
	tableName: 'tblMessages',
	idAttribute: 'messageId',
});

module.exports = {
	User: User,
	Message: Message
};